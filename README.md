# 14252-meeting-room-lights

Confluence page

https://cherry.atlassian.net/wiki/spaces/CMRB/overview

A node backend to operate Philips Hue lights from FireBase.

Currently there is one raspberry pi handling lights in sycamoure court and we will need to setup another one to handle the lights in elm court.

## Configuring project to work on local

```
get .env details from confluence and create a .env file in the root directory with the details.
npm i
npm start
```

## Raspberry Pi (Setting up new raspberry pi)

Install the recommended os (Raspbian)

Update Node by entering the following commands in console.

sudo su;
wget -O - https://raw.githubusercontent.com/audstanley/NodeJs-Raspberry-Pi/master/Install-Node.sh | bash;
exit;
node -v;

The current version of node should be V10.15

Download the project repo from bitbucket and then add the .env file with all the provided details

These details can be found in the confluence main info page for the internal app.

Once inside the project directory run:

npm i

and once everything has installed run

npm start

## Updating raspberry pi code

Push new code to bit bucket

Go on raspberry pi (plug it into a screen and add keyboard and mouse or access remotely using VNC viewer, details below). download pushed code and then run

npm i

and once everything is installed

npm start


## Setting up remote access to the Raspberry Pi

### Raspberry Pi

The first stage is to set up VNC server on the Raspberry Pi. (If this has already been done, skip to 'Your Machine' section)

On the raspberry pi, in the terminal, run the following:

```
sudo apt-get update

sudo apt-get install realvnc-vnc-server realvnc-vnc-viewer
```

Once that's done, type

```
sudo raspi-config
```

scroll down and select 'Interfacing options', then 'VNC'. (Use arrow keys, enter and escape to navigate)

Select 'Yes', when it asks you if you would like to enable the VNC server.

Open VNC (click on the VNC icon in the top-right corner). Then click the status menu and select licensing. Enter the VNC account email address and password - this is available on the third-party credentials confluence page. (you'll also have the option to name the raspberry pi, by default it's 'raspberrypi')

### Your Machine

Back on your machine (or the device from which you want to have remote access), download and install VNC viewer - https://www.realvnc.com/en/connect/download/viewer/

Sign in to VNC viewer - credentials are available on the third-party access confluence page

If you've not signed in before, you'll need to authorize the device. Sign in to cherriesdigital@gmail.com and find & accept the authorization email. 

Back in VNC viewer, you should now see a computer called raspberrypi has been added (the name might be different if you called it something else during VNC server setup)

Double click on raspberrypi and login using the VNC server details. By default these are:

username: pi
password: raspberry

And you should now have remote access

#### Useful Links:
https://lifehacker.com/how-to-control-a-raspberry-pi-remotely-from-anywhere-in-1792892937


## 2019 / 07 / 09 - BUG FIX

If lights don't work check that the .env contains the correct IP. Had to change the ip from 192.168.2.20 to 192.168.2.10

##�How the lights work

<!-- To do, add screenshots -->

The lights uses a bridge which essentially controls all the lights connected on the same internal ip address of the wifi network. You then connect to the bridge and call methods off the bridge with a number of what light to change.

## Adding a new light

Uncomment the commented out code in lightService to get the ip of the bridge, then add the correct ip to the .env file. The lights order will need to be seen based on order and changed at a later date. After this is done, the code will need to be updated on the raspberry pi. This was achieved by downloading the code off a git repo and then just manually running it.

## Lights info in console

![alt text](https://cherrymeeting.s3.eu-west-2.amazonaws.com/lightsInConsole.PNG)

```
On: Refers to if the light is on or

Hue: a colour or shade.

Saturation: saturation describes the intensity (purity) of that hue

Brightness: How bright the color will be
```

The hue api (Find login details in confluence)

https://developers.meethue.com/develop/hue-api/
