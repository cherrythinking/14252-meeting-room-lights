module.exports = {
  lightColors: {
    Busy: {
      bri: 120,
      sat: 0,
      hue: 45000    // blue hue, but 0 saturation, and 255 brightness (100%) makes it white
    },      
    Available: {
      bri: 200,
      sat: 255,
      hue: 25000    // green
    }, 
    "Ending soon": {
      bri: 200,
      sat: 255,
      hue: 65000    // red
    },
    "Starting now": {
      bri: 200,
      sat: 255,
      hue: 5000    // orange
    },
  },

  roomLightNumber: {
    Maraschino: 1,
    Morello: 2,
  },
};
