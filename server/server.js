require("dotenv").config();

const DatabaseFactory = require("../services/databaseService");
const db = new DatabaseFactory();
db.authorise();
