const firebase = require("firebase");

const HueLights = require("./lightService");
const constants = require("../config/constants");

const firebaseConfig = {
  apiKey: process.env.DB_API_KEY,
  authDomain: process.env.DB_AUTH_DOMAIN,
  databaseURL: process.env.DB_URL,
  projectId: process.env.DB_PROJECT_ID,
  storageBucket: "",
  messagingSenderId: process.env.DB_MESSAGE_SENDER_ID,
};

class DatabaseFactory {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.database = firebase.database();
    this.auth = firebase.auth();
    this.lights = new HueLights();
  }

  authorise() {
    this.auth.signInWithEmailAndPassword(process.env.DB_USER, process.env.DB_PASS).catch(error => {
      console.log(error.code, error.message);
    });

    this.auth.onAuthStateChanged(user => {
      if (user) {
        console.log("DB_USER_AUTHED");
        this.listenForRoomInfoChange();
      } else {
      }
    });
  }

  listenForRoomInfoChange() {
    this.database.ref("/roomInfo/").on("value", snapshot => {
      const roomNames = Object.keys(snapshot.val());

      roomNames.forEach(room => {
        if (room === "Morello" || room === "Maraschino") {
          const lightNumber = constants.roomLightNumber[room];
          const roomState = snapshot.val()[room].state;
          const lightColour = { bri: constants.lightColors[roomState].bri, sat: constants.lightColors[roomState].sat, hue: constants.lightColors[roomState].hue };
          this.lights.setLightColor(lightNumber, lightColour);
        }
      });
    });
  }
}

module.exports = DatabaseFactory;
