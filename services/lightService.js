const jshue = require("jshue");

class HueLights {
  constructor() {
    this.bridge = jshue().bridge(process.env.HUE_INTERNAL_IP);
    this.user = this.bridge.user(process.env.HUE_USER);
  }

  // UNCOMMENT TO GET THE LIGHTS INFO
  // getLightInfo() {
  //   this.user.getLights().then(console.log);
  // }

  setLightColor(lightNum, { bri, sat, hue }) {
    this.user
      .setLightState(lightNum, {
        on: true,
        bri,
        sat,
        hue,
      })
      .then(
        res => {
          console.log(res);
        },
        reason => {
          console.log(reason);
        }
      );
  }
}

module.exports = HueLights;
